# Lumberyard Woodland Asset Package(Unofficial User Guide)
![amazon-lumberyard](https://user-images.githubusercontent.com/18353476/28592053-ae35f67a-713c-11e7-9d2b-9fab8de0c94c.jpg)

Install Lumberyard: https://aws.amazon.com/lumberyard/

Lumberyard Documentation: https://docs.aws.amazon.com/lumberyard/latest/userguide/setting-up-system-requirements.html

Amazon GameDev Tutorials: https://gamedev.amazon.com/forums/tutorials

An Introduction to AWS Lumberyard Game Development: https://www.packtpub.com/books/content/introduction-aws-lumberyard-game-development

# Lumberyard Support for macOS(Preview release)

macOS Support Documentation: https://docs.aws.amazon.com/lumberyard/latest/userguide/osx-intro.html

# Lumberyard Support for Linux(Preview release)

https://docs.aws.amazon.com/lumberyard/latest/userguide/linux-intro.html

# System Requirements

Lumberyard requires the following hardware and software:

Windows 7 64-bit or Later

3GHz minimum quad-core processor

8 GB RAM minimum

2 GB minimum DX11 or later compatible video card

Nvidia driver version 368.81 or AMD driver version 16.15.2211 graphics card

60 GB minimum of free disk space

One or both of the following: (required to compile Lumberyard Editor and tools)

    Visual Studio 2013 Update 4 or later
    
    Visual Studio 2015 Update 3 or later
    
 # Setup and Configure Projects in Lumberyard
 
![1_launcher](https://user-images.githubusercontent.com/18353476/28295300-864421c4-6b14-11e7-8771-40a598086499.png)
![2_setupgems](https://user-images.githubusercontent.com/18353476/28333786-f2a032d8-6bad-11e7-97db-59fd6675c37d.png)
![samples-woodland-assets](https://user-images.githubusercontent.com/18353476/28646634-8f25cb4e-7217-11e7-8321-701cdfe4a224.png)
![ly-downloads_2](https://user-images.githubusercontent.com/18353476/28296953-0f5e0b0e-6b20-11e7-96fd-b45e4bbf4485.png)
![yakushima-forest](https://user-images.githubusercontent.com/18353476/28398080-5b5bce4c-6cba-11e7-9bf9-0cf3c8648368.png)
